<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class ExternalSiteTest extends DuskTestCase
{
    public function testLanding()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('http://v.kost.tv/l/PxVdymUw2n%2BiGobGFnV8wxBr7NyUlj83VWAX5VzP9Y3vTuI4APxeRzVZZtnLDyAL05Vm6yMMDxg2mZ5CGzqD1BPcdXaswcijo%2BGtVAz5If%2BRR%2BxDO5mi0yEihM1r2JsuHypR8NZYOURAxoZYQYecBbA1nbqYOHn5Oh1PpxI0T0M%3D');
            sleep(mt_rand(60, 120));
        });
    }
}
